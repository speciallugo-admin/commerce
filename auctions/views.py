from django.contrib.auth import authenticate, login, logout
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from flask_login import login_required

from .models import *


def index(request):
    listings = Listing.objects.filter(is_closed=False)
    context = {
        "listings": listings
    }

    return render(request, "auctions/index.html", context)


def listing(request, listing_id):
    listing = Listing.objects.get(pk=listing_id)
    is_in_watchlist = request.user in listing.watchlist.all()
    comments = listing.comments.all()
    is_creator = request.user.username == listing.owner.username
    context = {
        "listing": listing,
        "is_in_watchlist": is_in_watchlist,
        "is_creator": is_creator,
        "comments": comments
    }

    return render(request, "auctions/listing.html", context)


def category(request):
    category = request.POST["category"]
    listings = Listing.objects.filter(category=category)
    context = {
        "listings": listings
    }
    
    return render(request, "auctions/index.html", context)

@login_required
def watchlist(request):
    user = request.user
    listings = user.watchlist.all()
    context = {
        "listings": listings
    }

    return render (request, "auctions/watchlist.html", context)

@login_required
def addWatchlist(request, listing_id):
    user = request.user
    listing = Listing.objects.get(pk=listing_id)
    listing.watchlist.add(user)
    
    return HttpResponseRedirect(reverse("listing", args=(listing_id)))

@login_required
def deleteWatchlist(request, listing_id):
    user = request.user
    listing = Listing.objects.get(pk=listing_id)
    listing.watchlist.remove(user)

    return HttpResponseRedirect(reverse("listing", args=(listing_id)))

@login_required
def newBid(request, listing_id):
    listing = Listing.objects.get(pk=listing_id)
    currentBid = listing.bid.bid
    newBid = bid = int(request.POST["bid"])
    if newBid > currentBid:
        updateBid = Bid(newBid, user=request.user)
        updateBid.save()
        listing.bid = updateBid
        listing.save()

        return render(request, "auctions/listing.html", {
            "listing": listing,
            "message": "Bid was place successfully.",
            "updated": True
        })
    else:
        return render(request, "auctions/listing.html", {
            "listing": listing,
            "message": "New bid must be higher than current bid.",
            "updated": False
        })

@login_required
def newListing(request):
    if request.method == "POST":
        user = request.user
        title = request.POST["title"]
        description = request.POST["description"]
        image_URLs = request.POST["image_URLs"]
        category = request.POST["category"]

        bid = Bid(bid=int(request.POST["bid"]), user=user)
        bid.save()

        listing = Listing(creator=user, title=title, description=description, url=image_URLs, category=category, bid=bid)
        listing.save()

        return HttpResponseRedirect(reverse("index"))
    return render(request, "auctions/newListing.html")

@login_required
def categories(request):
    category = request.POST["category"]
    listings = Listing.objects.filter(category=category)
    context = {
        "listings": listings
    }

    return render(request, "auctions/categories.html", context)

@login_required
def comment(request, listing_id):
    user = request.user
    comment = request.POST["comment"]
    listing = listing = Listing.objects.get(pk=listing_id)
    newComment = Comment(commenter=user, comment=comment, listing=listing)
    newComment.save()

    return HttpResponseRedirect(reverse("listing", args=(listing_id)))
    
def closedListing(request):
    closedListing = Listing.objects.filter(is_closed=True)
    context = {
        "listings": closedListing
    }

    return render( request, "auctions/closedListing.html", context)

    
def closedAuction(request, listing_id):
    listing = Listing.objects.get(pk=listing_id)
    listing.is_closed =True
    listing.save()

    return HttpResponseRedirect(reverse("listing", args=("listing_id")))


def login_view(request):
    if request.method == "POST":

        # Attempt to sign user in
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)

        # Check if authentication successful
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse("index"))
        else:
            return render(request, "auctions/login.html", {
                "message": "Invalid username and/or password."
            })
    else:
        return render(request, "auctions/login.html")


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("index"))


def register(request):
    if request.method == "POST":
        username = request.POST["username"]
        email = request.POST["email"]

        # Ensure password matches confirmation
        password = request.POST["password"]
        confirmation = request.POST["confirmation"]
        if password != confirmation:
            return render(request, "auctions/register.html", {
                "message": "Passwords must match."
            })

        # Attempt to create new user
        try:
            user = User.objects.create_user(username, email, password)
            user.save()
        except IntegrityError:
            return render(request, "auctions/register.html", {
                "message": "Username already taken."
            })
        login(request, user)
        return HttpResponseRedirect(reverse("index"))
    else:
        return render(request, "auctions/register.html")
