from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone


class User(AbstractUser):
    pass

class Bid (models.Model):
    bid = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="bid")

    def __str__(self):
        return f"Bid of {self.bid} from {self.user}"

class Listing(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=60)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="listing", null=True, blank=True)
    is_closed = models.BooleanField(default=False, blank=True, null=True)
    description = models.CharField(max_length=300)
    bid = models.ForeignKey(Bid, on_delete=models.CASCADE, related_name="listing",default=None)
    urls = models.CharField(max_length=500)
    watchlist = models.ManyToManyField(User, blank=True, related_name="watchedListings")
    category = models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return f"{self.title}: {self.bid}"
class Comment(models.Model):
    comment = models.CharField(max_length=300)
    commenter = models.ForeignKey(User, on_delete=models.CASCADE, related_name="comments")
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name="comments")
    date_created = models.DateTimeField(default=timezone.now)
class Picture(models.Model):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name="get_picture")
    picture = models.ImageField(upload_to="images/")
    alt_text = models.CharField(max_length=100)