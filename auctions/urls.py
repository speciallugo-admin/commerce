from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("register", views.register, name="register"),
    path("categories", views.categories, name="categories"),
    path("newListing", views.newListing, name="newListing"),
    path("watchList", views.watchlist , name="watchlist"),
    path("closedListing", views.closedListing, name="closedListing"),
    path("<int:listing_id>", views.listing, name="listing"),
    path("<int:listing_id>/addWatchlist", views.addWatchlist, name="addWatchlist"),
    path("<int:listing_id>/deleteWatchlist", views.deleteWatchlist, name="deleteWatchlist"),
    path("<int:listing_id>/newBid", views.newBid, name="newBid"),
    path("<int:listing_id>/comment", views.comment, name="comment"),
    path("<int:listing_id>closedAuction", views.closedAuction, name="closedAuction")
]
